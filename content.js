"use strict"

browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (document.querySelector("#__FULLSCREEN__")) {
        return
    }

    const button = document.createElement("button")
    button.id = "__FULLSCREEN__"
    button.style = `
        z-index: 2147483647;
        position: fixed;
        width: 100%; height: 100%;
        top: 0; left: 0;
        background-color: rgba(0.86, 0.86, 0.86, 0.25);
        color: black;
        font-size: 250px;
    `
    button.onclick = () => {
        document.documentElement.requestFullscreen()
        button.remove()
    }
    button.appendChild(document.createTextNode("⛶"))

    document.body.appendChild(button)
})
